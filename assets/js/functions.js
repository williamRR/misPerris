const options = [
  {
    title: "Titulo 1",
    img: "assets/img/Bigotes.jpg",
    snippet: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus, fugiat...",
    body: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus, fugiat, placeat est magnam dolore esse explicabo vero eveniet cumque rem aliquid nobis. Laudantium quisquam cumque, ipsa perferendis laborum consequatur aspernatur numquam labore in! Omnis, optio soluta! Accusantium velit consequatur iure molestias est, provident, autem explicabo animi rem voluptas numquam iusto atque quibusdam inventore doloremque assumenda! .",
    footer: "Some Footer A"
  }, {
    title: "Título2",
    img: "assets/img/Chocolate.jpg",
    snippet: "Non nam quisquam ad expedita maxime! Non repellendus voluptate, amet consequatur...",
    body: "Non nam quisquam ad expedita maxime! Non repellendus voluptate, amet consequatur itaque tempore, ipsa aliquam exercitationem labore dolor culpa deserunt voluptas enim rem atque laudantium porro, ab expedita quam necessitatibus aperiam possimus cupiditate doloribus. Maxime inventore modi odio excepturi dolorem omnis labore, facilis corrupti corporis obcaecati. Debitis enim itaque modi veritatis cum illum recusandae reiciendis magni nihil expedita repudiandae, tempora hic fugiat similique! ",
    footer: "Some Footer B"
  }, {
    title: "Título3",
    img: "assets/img/Luna.jpg",
    snippet: "Fuga officiis iste neque nihil deleniti aperiam consequatur minima modi? Rerum...",
    body: "Fuga officiis iste neque nihil deleniti aperiam consequatur minima modi? Rerum nihil cum temporibus quidem inventore voluptatibus accusantium at possimus consequuntur aspernatur tenetur ad, delectus, facere odit ex sapiente, mollitia adipisci enim quisquam nisi nobis vitae? Non possimus quisquam eum commodi atque repellat incidunt saepe? Nobis delectus maiores illo natus expedita dignissimos tenetur corporis quisquam quis excepturi minima voluptas minus ipsa sit, nihil asperiores aliquam praesentium. Maiores modi non minima nam pariatur voluptate doloribus perferendis. At quis libero obcaecati saepe a distinctio fuga, repudiandae aspernatur voluptatibus ea fugit. .",
    footer: "Some Footer C"
  }, {
    title: "Título4",
    img: "assets/img/Pexel.jpg",
    snippet: "Architecto ipsum ipsam dolor ad pariatur non aspernatur iure, ut quam quasi...",
    body: "Architecto ipsum ipsam dolor ad pariatur non aspernatur iure, ut quam quasi, quod magni aut vel autem sed soluta hic. Mollitia officia, iste doloribus sunt, culpa saepe quod molestias consequuntur sed sit voluptas atque a architecto doloremque temporibus debitis numquam adipisci. Consequuntur, totam. Tempora ipsam nulla dignissimos voluptas assumenda iusto, ad iste repellat accusantium minus similique, molestias laudantium dolores itaque quae distinctio harum eos recusandae repudiandae beatae! Eos voluptatibus soluta, debitis tenetur voluptatum hic quo a! Saepe nesciunt repellendus dignissimos aut officia nulla aliquid quos suscipit exercitationem hic sequi incidunt voluptate et illo ut animi iure delectus nobis omnis at, nam consectetur? .",
    footer: "Some Footer D"
  }, {
    title: "Título5",
    img: "assets/img/Wifi.jpg",
    snippet: "PriVitae nostrum reprehenderit quaerat a assumenda, ducimus natus id eos...",
    body: "PriVitae nostrum reprehenderit quaerat a assumenda, ducimus natus id eos molestias, magni aspernatur asperiores. Vitae voluptas aliquid quisquam minima, perspiciatis autem eligendi quo molestias, qui nobis quia similique veniam? .",
    footer: "Some Footer E"
  }, {
    title: "Título6",
    img: "assets/img/Maya.jpg",
    snippet: "Alias modi molestias magnam perspiciatis, tenetur suscipit nesciunt eius",
    body: "Alias modi molestias magnam perspiciatis, tenetur suscipit nesciunt eius exercitationem reprehenderit sequi consequuntur sed libero, dolores ratione? Provident, asperiores sunt totam a incidunt excepturi dolorum hic reiciendis in maiores inventore corporis molestiae? Unde quibusdam nam fugit nihil architecto esse, molestiae animi. Id eius laborum iure ea possimus quas rem consectetur quia voluptatibus nesciunt molestias similique fugit, soluta deleniti modi assumenda odit iste reiciendis..",
    footer: "Some Footer F"
  }
]

$(document).ready(function () {

  /**
   * MÉTODO PARA VALIDAR RUT
   * @param {string} campo - RUT INGRESADO POR USUARIO
   */
  const validaRut = campo => {
    if (campo.length == 0) { return false; }
    if (campo.length < 8) { return false; }

    campo = campo.replace('-', '')
    campo = campo.replace(/\./g, '')

    var suma = 0;
    var caracteres = "1234567890kK";
    var contador = 0;
    for (var i = 0; i < campo.length; i++) {
      u = campo.substring(i, i + 1);
      if (caracteres.indexOf(u) != -1)
        contador++;
    }
    if (contador == 0) { return false }

    var rut = campo.substring(0, campo.length - 1)
    var drut = campo.substring(campo.length - 1)
    var dvr = '0';
    var mul = 2;

    for (i = rut.length - 1; i >= 0; i--) {
      suma = suma + rut.charAt(i) * mul
      if (mul == 7) mul = 2
      else mul++
    }
    res = suma % 11
    if (res == 1) dvr = 'k'
    else if (res == 0) dvr = '0'
    else {
      dvi = 11 - res
      dvr = dvi + ""
    }
    if (dvr != drut.toLowerCase()) { return false; }
    else { return true; }
  }

  /**
   * MÉTODO PARA LIMPIAR FORMULARIO
   */
  const cleaner = () => {
    $("#contactForm").validate().resetForm();
    $("#contactForm")[0].reset();
  }

  /**
   * CALCULANDO EDAD
   * @param {date} birthDay - from imput
   */
  const _calcAge = (birthDay) => {
    debugger
    var date = birthDay
    var today = new Date();

    var timeDiff = Math.abs(today.getTime() - date.getTime());
    var age1 = Math.ceil(timeDiff / (1000 * 3600 * 24)) / 365;
    if (age1 < 10 && age1 > 80) return false
    else return true;
  }

/**
 * LISTENER PARA EL BOTÓN DE RESET FORM
 */
  $(document).on('click', "#resetForm", function () {
    cleaner()
  });

  /**
   * MÉTODO QUE RECORRE UN ARRAY Y DESPLIEGA CARDS CON SUS PROPS
   */
  $.each(options, function (i) {
    let templateString = ' <div class="col-md-12 col-lg-4 col-sm-3 mt-3"> <div class="card text-white bg-dark rounded p-3"><div class="card-header-first rounded pb-2" data-toggle="modal" data-target="#cardModal" data-body="' + options[i].body + '"><h2 card-header-title text-white pt-3>' + options[i].title + '</h2>          </div>          <div class="card-body text-center">              <img class="card-img-top" src="' + options[i].img + '" width="40" alt="Card image cap">     ' + options[i].snippet + '     </div>          <div class="card-footer bg-transparent border-success">' + new Date() + '</div>  </div>    </div> '
    $('#cards').append(templateString)
  })

  birthDate.max = new Date().toISOString().split("T")[0];

  /**
   * MÉTODO QUE AÑADE VALIDADOR DE RUT AL JQUERY VALIDATOR
   */
  jQuery.validator.addMethod("rut", function (value, element) {
    return this.optional(element) || validaRut(value);
  });

  /**
   * MÉTODO QUE AÑADE VALIDADOR DE EDAD AL JQUERY VALIDATOR
   */
  jQuery.validator.addMethod("edad", function (value, element) {
    return this.optional(element) || _calcAge(value)
});

/**
 * JQUERY.VALIDATOR
 */
$("#contactForm").validate({
  onkeyup: function (element) {
    $(element).valid();
  },
  rules: {
    name: {
      minlength: 5,
      required: true,
    },
    lastname: {
      minlength: 5,
      required: true,
    },
    email: {
      email: true,
      required: true,
    },
    confirmEmail: {
      email: true,
      required: true,
      equalTo: "#email"
    },
    rut: {
      rut: true,
      required: true,
    },
    birthDate: {
      required: true,
      edad: true,
    },
    phone: {
      required: true,
      digits: true,
      minlength: 9,
      maxlength: 9
    },
  },
  messages: {
    name: {
      minlength: "El nombre debería tener al menos cinco caracteres.",
      required: "El campo es obligatorio.",
    },
    lastname: {
      minlength: "El nombre debería tener al menos cinco caracteres.",
      required: "El campo es obligatorio.",
    },
    email: {
      email: "Formato no válido.",
      required: "El campo es obligatorio.",
    },
    confirmEmail: {
      equalTo: "Debe ingresar el mismo correo",
      email: "Formato no válido.",
      required: "El campo es obligatorio.",
    },
    rut: {
      required: "El campo es obligatorio.",
      rut: "Revise que esté bien escrito"
    },
    birthDate: {
      required: "El campo es obligatorioo.",
      edad: "No parece una edad válida."
    },
    phone: {
      required: "El campo es obligatorio.",
      digits: "Sólo números.",
      minlength: "Ingrese con formato de nueve dígitos.",
      maxlength: "Ingrese con formato de nueve dígitos."
    },
  }
});

/**
 * MÉTODO QUE MANDA UN VALOR RECIBIDO AL MODAL
 */
$('#cardModal').on('show.bs.modal', function (e) {
  var cardBody = $(e.relatedTarget).data("body");
  $(this).find(".modal-body").text(cardBody);
});

/**
 * MÉTODO PARA LIMPIAR MODAL ON BLUR
 */
$('#formModal').on('hide.bs.modal', function (e) {
  cleaner()
});
})